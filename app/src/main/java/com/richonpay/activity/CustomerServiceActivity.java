package com.richonpay.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.richonpay.App;
import com.richonpay.R;
import com.richonpay.api.API;
import com.richonpay.api.APICallback;
import com.richonpay.api.BadRequest;
import com.richonpay.base.ToolbarActivity;
import com.richonpay.model.APIModels;
import com.richonpay.model.APIResponse;

import butterknife.BindView;

public class CustomerServiceActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.csNoteTV)
    TextView csNoteTV;
    @BindView(R.id.emailTV)
    TextView emailTV;
    @BindView(R.id.phoneTV)
    TextView phoneTV;
    @BindView(R.id.waTV)
    TextView waTV;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_customer_service;
    }

    @Override
    protected void onViewCreated() {
        getContactUs();
        tvTitle.setText(R.string.customer_service);
    }

    public void getContactUs() {
        try {
            API.service().getContactUs().enqueue(new APICallback<APIResponse>(this) {
                @Override
                protected void onSuccess(APIResponse response) {
                    if (response.getData() != null) {
                        APIModels data = response.getData();
                        phoneTV.setText(data.getPhone());
                        emailTV.setText(data.getEmail());
                        waTV.setText(data.getWhatsapp());
                        csNoteTV.setText(data.getCs());
                    }
                }

                @Override
                protected void onError(BadRequest error) {
                }
            });
        } catch (Exception exception) {

            Log.e("fetchProfile", "" + exception);
        }
    }

}
