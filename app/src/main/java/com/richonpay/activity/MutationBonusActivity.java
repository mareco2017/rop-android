package com.richonpay.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richonpay.R;
import com.richonpay.adapter.MutationAdapter;
import com.richonpay.api.API;
import com.richonpay.api.APICallback;
import com.richonpay.api.BadRequest;
import com.richonpay.base.ToolbarActivity;
import com.richonpay.model.APIResponse;
import com.richonpay.model.BankAccount;
import com.richonpay.model.UpgradeRequest;
import com.richonpay.utils.Extension;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MutationBonusActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvBalance)
    TextView tvBalance;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.llTransfer)
    LinearLayout llTransfer;
    private double totalAmount = 0;
    private MutationAdapter mAdapter;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mutation_bonus;
    }

    @Override
    protected void onViewCreated() {
        mAdapter = new MutationAdapter(this, new MutationAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int id, UpgradeRequest upgradeRequest) {
            }
        });
        tvTitle.setText(R.string.mutasi_bonus);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        getTransactionList();
    }

    private void getTransactionList() {
        try {
            API.service().getTransaction( "3", "15,19").enqueue(new APICallback<APIResponse>(this) {
                @Override
                protected void onSuccess(APIResponse response) {

                    if (response.getData() != null) {
                        if (response.getData().getUpgradeRequests() != null) {
                            List<UpgradeRequest> data = new ArrayList<>();
                            for(int l=0; l< response.getData().getUpgradeRequests().size(); l++){
                                if (response.getData().getUpgradeRequests().get(l).getClaimedAt() == null) {
                                    data.add(response.getData().getUpgradeRequests().get(l));

                                }
                                totalAmount += response.getData().getUpgradeRequests().get(l).getAmount();
                            }
                            tvBalance.setText(Extension.priceFormat(totalAmount));
                            if (data.size() == 0) {
                                llTransfer.setVisibility(View.GONE);
                            }
                            else {
                                llTransfer.setVisibility(View.VISIBLE);
                            }
                            mAdapter.setItems(data);
                        }
                    }
                }

                @Override
                protected void onError(BadRequest error) {
                }
            });
        } catch (Exception exception) {
            Log.e("getTransactionList", "" + exception);
        }
    }

    @OnClick(R.id.btnTransfer)
    void register() {
        Intent intent = new Intent(getBaseContext(), UserBankAccountActivity.class);
        intent.putExtra("WITHDRAW", true);
        intent.putExtra("AMOUNT", totalAmount);
        startActivityForResult(intent, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mutation, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mutation: {
                Intent intent = new Intent(MutationBonusActivity.this, MutationActivity.class);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                boolean successWithdraw = data.getBooleanExtra("WITHDRAW", false);
                if (successWithdraw) {
                    List<UpgradeRequest> tmp = new ArrayList<>();
                    mAdapter.setItems(tmp);
                    tvBalance.setText(Extension.priceFormat(0));
                }
            }
        }
    }

}