package com.richonpay.activity;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richonpay.R;
import com.richonpay.api.API;
import com.richonpay.api.APICallback;
import com.richonpay.api.BadRequest;
import com.richonpay.base.ToolbarActivity;
import com.richonpay.model.APIModels;
import com.richonpay.model.APIResponse;
import com.richonpay.model.UpgradeRequest;
import com.richonpay.utils.Extension;

import org.parceler.Parcels;

import butterknife.BindView;

public class ActivityTransactionDetail extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvTransactionCode)
    TextView tvTransactionCode;
    @BindView(R.id.tvAmount)
    TextView tvAmount;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvBankTitle)
    TextView tvBankTitle;
    @BindView(R.id.tvBank)
    TextView tvBank;
    @BindView(R.id.tvCustomerId)
    TextView tvCustomerId;
    @BindView(R.id.llCustomer)
    LinearLayout llCustomer;

    private UpgradeRequest data;


    @Override
    protected int getContentViewResource() {
        return R.layout.activity_transaction_detail;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(R.string.detail_transaksi);
        if (getIntent() != null) {
            data = Parcels.unwrap(getIntent().getParcelableExtra("DATA"));

            if (data.getType() == 4) {
                tvBankTitle.setText(R.string.nomor_handphone);
                llCustomer.setVisibility(View.GONE);
                tvBank.setText(data.getOptions().getPhoneNumber());
            }
            else if (data.getType() == 0) {
                tvBankTitle.setText(R.string.bank_pembayaran);
                llCustomer.setVisibility(View.GONE);
                tvBank.setText(data.getVirtual().getDestBankAccount().getBank().getAbbr() + " / " + data.getVirtual().getDestBankAccount().getAccountNo());
            }
            else {
                tvBankTitle.setText(R.string.no_meter);
                tvBank.setText(data.getOptions().getMeterNo());
                tvCustomerId.setText(data.getOptions().getCustomerName());
            }

            tvTransactionCode.setText(String.valueOf(data.getReferenceNo()));

            tvDate.setText(Extension.receiptDetailDateFormat.format(data.getCreatedAt()));
            int paket = data.getType();
            tvAmount.setText(getTypeDescription(paket) + " " + Extension.priceFormat(data.getTotal()));
            switch (data.getStatus()) {
                case UpgradeRequest.PENDING:
                    tvStatus.setText(this.getString(R.string.waiting));
                    tvStatus.setBackgroundResource(R.drawable.status_waiting);
                    break;

                case UpgradeRequest.WAITING_PAYMENT:
                    tvStatus.setText(this.getString(R.string.waiting));
                    tvStatus.setBackgroundResource(R.drawable.status_waiting);
                    break;

                case UpgradeRequest.ON_PROCESS:
                    tvStatus.setText(this.getString(R.string.processing_allcaps));
                    tvStatus.setBackgroundResource(R.drawable.status_process);
                    break;

                case UpgradeRequest.ACCEPTED:
                    tvStatus.setText(this.getString(R.string.successful));
                    tvStatus.setBackgroundResource(R.drawable.status_success);
                    break;

                case UpgradeRequest.DECLINED:
                    tvStatus.setText(this.getString(R.string.declined_allcaps));
                    tvStatus.setBackgroundResource(R.drawable.status_cancelled);
                    break;

                case UpgradeRequest.CANCELLED:
                    tvStatus.setText(this.getString(R.string.cancel_allcaps));
                    tvStatus.setBackgroundResource(R.drawable.status_cancelled);
                    break;

                case UpgradeRequest.REFUNDED:
                    tvStatus.setText(this.getString(R.string.refund_allcaps));
                    tvStatus.setBackgroundResource(R.drawable.status_refund);
                    break;
                default:
                    break;
            }
        }


    }

    private String getTypeDescription(int type) {
        switch (type) {
            case 1:
                return "Withdraw";
            case 2:
                return "PLN";
            case 3:
                return "PLN";
            case 4:
                return "Pulsa";
            case 5:
                return "Pulsa";
            case 6:
                return "Isi Saldo";
            case 7:
                return "Pay";
            case 8:
                return "Transfer";
            case 9:
                return "Split";
            case 10:
                return "Cashback";
            case 11:
                return "Game";
            case 12:
                return "Freeze";
            case 13:
                return "Unfreeze";
            case 14:
                return "Withdraw";
            case 15:
                return "Bonus Sponsor";
            case 16:
                return "BPJS";
            case 17:
                return "Reserval";
            case 18:
                return "Upgrade Paket";
            case 19:
                return "Bonus Pairing";
            default:
                return "";
        }
    }

}
