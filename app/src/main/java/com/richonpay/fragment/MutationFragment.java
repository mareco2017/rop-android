package com.richonpay.fragment;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import com.richonpay.R;
import com.richonpay.adapter.MutationAdapter;
import com.richonpay.api.API;
import com.richonpay.api.APICallback;
import com.richonpay.api.BadRequest;
import com.richonpay.base.BaseFragment;
import com.richonpay.model.APIResponse;
import com.richonpay.model.TransactionOrderDetail;
import com.richonpay.model.UpgradeRequest;
import com.richonpay.utils.Extension;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.richonpay.activity.MutationActivity.TYPE_BALANCE;
import static com.richonpay.activity.MutationActivity.TYPE_BONUS_PENDING;
import static com.richonpay.activity.MutationActivity.TYPE_BONUS_SUCCESS;
import static com.richonpay.activity.MutationActivity.TYPE_POINT;

public class MutationFragment extends BaseFragment {


    @BindView(R.id.tvBalance)
    TextView tvBalance;
    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;
    @BindView(R.id.llHeader)
    LinearLayout llHeader;
    @BindView(R.id.llTransfer)
    LinearLayout llTransfer;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    private double totalAmount = 0;
    private MutationAdapter mAdapter;


    private int mutationType = TYPE_BALANCE;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mutation_bonus;
    }

    @Override
    protected void onViewCreated() {
        try {
            mutationType = getArguments().getInt("TYPE", TYPE_BALANCE);
            llTransfer.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
            if (mutationType == 2) {
                llHeader.setVisibility(View.GONE);
            }
            else {
                tvHeaderTitle.setText(R.string.total_bonus);
            }
            mAdapter = new MutationAdapter(getContext(), new MutationAdapter.OnItemClickListener() {
                @Override
                public void onClick(View view, int id, UpgradeRequest upgradeRequest) {
                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(mAdapter);

            getTransactionList();
        } catch (Exception exception) {
            Log.e("onViewCreated", "" + exception);
        }
    }

    private void getTransactionList() {
        try {
            Integer claimed = 0;
            if (mutationType == 3) {
                claimed = 1;
            }
            API.service().getStatement( claimed).enqueue(new APICallback<APIResponse>(getContext()) {
                @Override
                protected void onSuccess(APIResponse response) {
                    if (response.getData() != null) {
                        if (response.getData().getStatements() != null) {
                            if (mutationType == 3) {
                                for (int l = 0; l < response.getData().getStatements().size(); l++) {
                                    totalAmount += response.getData().getStatements().get(l).getAmount();
                                }
                                tvBalance.setText(Extension.priceFormat(totalAmount));
                            }
                            Log.e("asd1123123wadwd",": "+ response.getData().getStatements().size());
                            mAdapter.setItems(response.getData().getStatements());
                        }
                    }
                }

                @Override
                protected void onError(BadRequest error) {
                }
            });
        } catch (Exception exception) {
            Log.e("getTransactionList", "" + exception);
        }
    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_COLLAPSED:
                    break;
                case BottomSheetBehavior.STATE_DRAGGING:
                    break;
                case BottomSheetBehavior.STATE_EXPANDED:
                    break;
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;
                case BottomSheetBehavior.STATE_SETTLING:

                    break;
                default:
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };


}
