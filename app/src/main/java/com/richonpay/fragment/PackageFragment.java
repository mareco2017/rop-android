package com.richonpay.fragment;

import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.richonpay.R;
import com.richonpay.base.BaseFragment;
import com.richonpay.utils.Extension;

import butterknife.BindView;

public class PackageFragment extends BaseFragment {

    @BindView(R.id.flBackground)
    RelativeLayout flBackground;
    @BindView(R.id.ivPackage)
    ImageView ivPackage;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.llJoin)
    LinearLayout llJoin;
    @BindView(R.id.tvJoinFee)
    TextView tvJoinFee;
    @BindView(R.id.tvNetworkCashback)
    TextView tvNetworkCashback;
    @BindView(R.id.tvIncomePotential)
    TextView tvIncomePotential;
    @BindView(R.id.tvBonus)
    TextView tvBonus;
    @BindView(R.id.tvDetail)
    TextView tvDetail;

    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_package;
    }

    @Override
    protected void onViewCreated() {
        ivPackage.bringToFront();
        try {
            int packageType = getArguments().getInt("PACKAGE", 0);
            double fee = getArguments().getDouble("FEE", 0);
            int cashback = getArguments().getInt("CASHBACK", 0);
            double income = getArguments().getDouble("INCOME", 0);
            String bonus = getArguments().getString("BONUS", "");

            switch (packageType) {
                case 0:
                    ivPackage.setImageResource(R.drawable.silver);
                    flBackground.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.packageSilverBackground));
                    break;
                case 1:
                    ivPackage.setImageResource(R.drawable.gold);
                    flBackground.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.packageGoldBackground));
                    break;
                case 2:
                    ivPackage.setImageResource(R.drawable.platinum);
                    flBackground.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.packagePlatinumBackground));
                    break;
            }

            tvJoinFee.setText(Extension.priceFormat(fee));
            tvNetworkCashback.setText(String.valueOf(cashback + "%"));
            tvIncomePotential.setText(Extension.priceFormat(income));
            tvBonus.setText(String.valueOf(bonus));

        } catch (Exception exception) {
            Log.e("onViewCreated", "" + exception);
        }
    }
}
