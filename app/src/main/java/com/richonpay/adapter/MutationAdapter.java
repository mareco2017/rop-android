package com.richonpay.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richonpay.R;
import com.richonpay.base.BaseAdapter;
import com.richonpay.model.TransactionOrderDetail;
import com.richonpay.model.UpgradeRequest;
import com.richonpay.utils.Extension;

import java.util.Date;

import butterknife.BindView;

public class MutationAdapter extends BaseAdapter<UpgradeRequest, MutationAdapter.ViewHolder> {
    private final MutationAdapter.OnItemClickListener mListener;
    private Context context;

    public MutationAdapter(Context context, MutationAdapter.OnItemClickListener onItemClickListener) {
        this.context = context;
        this.mListener = onItemClickListener;
    }

    @Override
    protected MutationAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new MutationAdapter.ViewHolder(inflater.inflate(R.layout.item_mutation, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MutationAdapter.ViewHolder holder, int position) {

        final UpgradeRequest item = getItem(position);

        holder.tvTransactionDate.setText(Extension.receiptDetailDateFormat.format(item.getCreatedAt()));
        if (item.getReferenceNo() != null) {
            holder.tvTransactionFor.setText(item.getReferenceNo());
            holder.tvTransactionType.setVisibility(View.GONE);
        } else {
            holder.tvTransactionFor.setText("Statement ID:" + item.getStatementNo());
            holder.tvTransactionType.setText(item.getBankAccount().getAccountNo() + " / " + item.getBankAccount().getAccountName());
        }
        holder.tvAmount.setText(Extension.priceFormat(item.getAmount()));


    }

    public interface OnItemClickListener {
        void onClick(View view, int position, UpgradeRequest transaction);
    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder {

        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvTransactionFor)
        TextView tvTransactionFor;
        @BindView(R.id.tvTransactionType)
        TextView tvTransactionType;
        @BindView(R.id.tvTransactionDate)
        TextView tvTransactionDate;

        public ViewHolder(View view) {
            super(view);
        }
    }
}