package com.richonpay.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

@Parcel(Parcel.Serialization.BEAN)
public class UpgradeRequest {
    private int id;
    private int status;
    private String code;
    private double price;
    @SerializedName("package")
    private int packageType;
    private OptionModel options;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("expired_at")
    private Date expiredAt;
    private VirtualModel virtual;
    private  int type;
    @SerializedName("reference_number")
    private String referenceNo;
    private double total;
    private double amount;
    @SerializedName("statement_no")
    private String statementNo;
    @SerializedName("bank_account")
    private  BankAccount bankAccount;

    @SerializedName("claimed_at")
    private Date claimedAt;

    public static final int PENDING = 0;
    public static final int WAITING_PAYMENT = 1;
    public static final int ON_PROCESS = 2;
    public static final int ACCEPTED = 3;
    public static final int DECLINED = 4;
    public static final int CANCELLED = 5;
    public static final int REFUNDED = 6;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPackageType() {
        return packageType;
    }

    public void setPackageType(int packageType) {
        this.packageType = packageType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Date expiredAt) {
        this.expiredAt = expiredAt;
    }

    public OptionModel getOptions() {
        return options;
    }

    public void setOptions(OptionModel options) {
        this.options = options;
    }

    public VirtualModel getVirtual() {
        return virtual;
    }

    public void setVirtual(VirtualModel virtual) {
        this.virtual = virtual;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatementNo() {
        return statementNo;
    }

    public void setStatementNo(String statementNo) {
        this.statementNo = statementNo;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Date getClaimedAt() {
        return claimedAt;
    }

    public void setClaimedAt(Date claimedAt) {
        this.claimedAt = claimedAt;
    }
}
